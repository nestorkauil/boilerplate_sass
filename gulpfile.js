var fs           = require('fs');
var path         = require('path');
var gulp         = require('gulp');
var connect      = require('gulp-connect');
var copy         = require('gulp-copy');
var postcss      = require('gulp-postcss');
var rename       = require('gulp-rename');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var pixrem       = require('pixrem');
var comments     = require('postcss-discard-comments');
var cssnano      = require('cssnano');

var paths = {
    here       : './',
    dist       : 'dist',
    src        : 'src',
    assets     : '/assets',
    fonts      :  {
       src     :  'fonts/**/**',
       dest    :  'src/assets/fonts/',
    },
    scss_source: './scss/**/*.scss',
    scss       : './scss/main*'
};


gulp.task('dev', ['copy','scss','server', 'watch']);

gulp.task('deploy', ['scss-min', 'js-min']);

gulp.task('copy',['copy:fonts']);

gulp.task('copy:fonts', function () {
    return gulp.src(paths.fonts.src)
        .pipe(copy(paths.fonts.dest,{
            prefix: 1
        }));
});

gulp.task('watch', function () {
    gulp.watch([paths.scss_source],['scss']);
    gulp.watch(['src/*.html'], ['html']);
    //gulp.watch(Paths.JS,   ['js']);
});

gulp.task('html',function(){
   gulp.src(paths.src+'/*.html')
       .pipe(gulp.dest(paths.src))
       .pipe(connect.reload())
});

// Tareas para levantar el servidor livereload
gulp.task('server', function () {
    connect.server({
        root: paths.src,
        livereload: true
    })
});

// Tareas para desarrollo
gulp.task('scss', function () {
    return gulp.src(paths.scss)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([pixrem(),autoprefixer()]))
        .pipe(sourcemaps.write(paths.here))
        .pipe(gulp.dest(paths.src+'/'+paths.assets+'/'+paths.dist))
        .pipe(connect.reload());
});

gulp.task('js', function () {
    return gulp.src(Paths.JS)
        .pipe(concat('toolkit.js'))
        .pipe(replace(/^(export|import).*/gm, ''))
        .pipe(babel({
                "compact" : false,
                "presets": [
                    [
                        "es2015",
                        {
                            "modules": false,
                            "loose": true
                        }
                    ]
                ],
                "plugins": [
                    "transform-es2015-modules-strip"
                ]
            }))
        .pipe(wrapper({
            header: banner +
            "\n" +
            jqueryCheck +
            "\n" +
            jqueryVersionCheck +
            "\n+function () {\n",
            footer: '\n}();\n'
        }))
        .pipe(gulp.dest(Paths.DIST))
});

// Tareas finales para distribucion final (PROD)

gulp.task('scss-min', function () {
    return gulp.src(paths.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            pixrem(),
            autoprefixer(),
            comments({
                removeAll: true
            }),
            cssnano({
                safe: true
            })
        ]))
        //.pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.src+'/'+paths.assets+'/'+paths.dist))
});

gulp.task('js-min', ['js'], function () {
    return gulp.src(Paths.DIST_TOOLKIT_JS)
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.dist))
});

